package com.parkit.parkingsystem.integration;

import com.parkit.parkingsystem.config.DataBaseConfig;
import com.parkit.parkingsystem.util.DatabasePropertiesLoader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class DatabaseConfigIT {

    private DataBaseConfig dataBaseConfig;

    @BeforeEach
    public void setup() throws IOException {
        DatabasePropertiesLoader databasePropertiesLoader = new DatabasePropertiesLoader();
        dataBaseConfig = new DataBaseConfig(databasePropertiesLoader.getDbProperties());
    }

    @Test
    public void connect_connectDB_shouldNotThrowException() throws SQLException, ClassNotFoundException {
            dataBaseConfig.getConnection();
    }

    @Test
    public void connect_disconnectDB_shouldNotThrowException() throws SQLException, ClassNotFoundException {
        Connection con = dataBaseConfig.getConnection();
        Throwable thrown = catchThrowable(() ->  dataBaseConfig.closeConnection(con));
        assertThat(thrown).isNull();
    }

    @Test
    public void connect_closePreparedStatement_shouldNotThrowException() throws SQLException, ClassNotFoundException {
        Connection con = dataBaseConfig.getConnection();
        PreparedStatement preparedStatement = con.prepareStatement("SELECT 1");
        Throwable thrown = catchThrowable(() ->  dataBaseConfig.closePreparedStatement(preparedStatement));
        assertThat(thrown).isNull();
        dataBaseConfig.closePreparedStatement(preparedStatement);
        dataBaseConfig.closeConnection(con);
    }

    @Test
    public void connect_closeResultSet_shouldNotThrowException() throws SQLException, ClassNotFoundException {
        Connection con = dataBaseConfig.getConnection();
        PreparedStatement preparedStatement = con.prepareStatement("SELECT 1");
        ResultSet resultSet = preparedStatement.getResultSet();
        Throwable thrown = catchThrowable(() ->  dataBaseConfig.closeResultSet(resultSet));
        assertThat(thrown).isNull();
        dataBaseConfig.closeResultSet(resultSet);
        dataBaseConfig.closePreparedStatement(preparedStatement);
        dataBaseConfig.closeConnection(con);
    }
}
