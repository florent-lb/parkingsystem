package com.parkit.parkingsystem.exception;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Exception must be throw when a vehicle is inside a parking and we try to calc is fare
 */
@RequiredArgsConstructor
public class VehicleAlwaysInException extends RuntimeException
{

    @NonNull
    private final String vehiculeRegNumber;

    @Override
    public String getMessage() {
        return "The vehicule " + vehiculeRegNumber + " is always in parking !";
    }
}
