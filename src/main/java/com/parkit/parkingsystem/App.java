package com.parkit.parkingsystem;

import com.parkit.parkingsystem.config.DataBaseConfig;
import com.parkit.parkingsystem.service.InteractiveShell;
import com.parkit.parkingsystem.service.impl.ConsoleDisplayService;
import com.parkit.parkingsystem.util.DatabasePropertiesLoader;
import com.parkit.parkingsystem.util.InputReaderUtil;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;

/**
 * Launcher of the application  
 */
@Log4j2(topic = "App")
public class App {
    public static void main(String args[])
    {
        logger.info("Initializing Parking System");
        DatabasePropertiesLoader databasePropertiesLoader = new DatabasePropertiesLoader();
        try {
            DataBaseConfig dbConfig = new DataBaseConfig(databasePropertiesLoader.getDbProperties());
            InteractiveShell shell = new InteractiveShell(new InputReaderUtil(),new ConsoleDisplayService(),dbConfig);
            shell.loadInterface();
        } catch (IOException e) {
            logger.error("Impossible to load the db file, verify a file database.properties is present at root");
        }

        logger.info("Exiting Parking System");
    }
}
