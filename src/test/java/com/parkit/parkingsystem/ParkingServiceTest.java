package com.parkit.parkingsystem;

import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.dao.impl.ParkingSpotDAO;
import com.parkit.parkingsystem.dao.impl.TicketDAO;
import com.parkit.parkingsystem.exception.NoSpotAvailableException;
import com.parkit.parkingsystem.model.ParkingSpot;
import com.parkit.parkingsystem.model.Ticket;
import com.parkit.parkingsystem.service.FareCalculatorService;
import com.parkit.parkingsystem.service.ParkingService;
import com.parkit.parkingsystem.service.contract.DisplayService;
import com.parkit.parkingsystem.util.InputReaderUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ParkingServiceTest {

    private static ParkingService parkingService;

    @Mock
    private InputReaderUtil inputReaderUtil;
    @Mock
    private ParkingSpotDAO parkingSpotDAO;
    @Mock
    private TicketDAO ticketDAO;
    @Mock
    private DisplayService displayService;
    @Mock
    private FareCalculatorService fareCalculatorService;

    @BeforeEach
    private void setUpPerTest() {
        parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO, fareCalculatorService, displayService);

    }

    @ParameterizedTest
    @EnumSource(ParkingType.class)
    @DisplayName("When a vehicle of type {0} incoming")
    public void park_whenVehicleIncomming_shouldReturnParkingSpotCopiedWithVehicule(ParkingType parkingType) {
        when(inputReaderUtil.readSelection()).thenReturn(parkingType.ordinal() + 1);
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenReturn("ABCDEF");
        when(parkingSpotDAO.getNextAvailableSlot(parkingType)).thenReturn(1);
        //Initialized Ticket Incomming
        Ticket ticket = new Ticket();
        ParkingSpot parkingSpot = new ParkingSpot(1, parkingType, false);
        ticket.setInTime(LocalDateTime.now());
        ticket.setParkingSpot(parkingSpot);
        ticket.setVehicleRegNumber("ABCDEF");

        //GET
        when(ticketDAO.isRecurrentUser(anyString())).thenReturn(parkingType.ordinal()%2 == 0);

        //WHEN
        parkingService.processIncomingVehicle();
        //ASSERT
        verify(parkingSpotDAO, Mockito.times(1)).updateParking(any(ParkingSpot.class));
        assertThat(ticket.getVehicleRegNumber()).isEqualTo("ABCDEF");
        assertThat(ticket.getPrice()).isEqualTo(0.0);
        assertThat(ticket.getOutTime()).isNull();
        verify(ticketDAO, times(1)).saveTicket(any(Ticket.class));
    }

    @Test
    @DisplayName("When a vehicle incoming with wrong vehicle number")
    public void park_whenWrongVehicleNumber_shouldDisplayMessageIncorrectEntry() {
        //GET
        when(inputReaderUtil.readSelection()).thenReturn(1);
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenThrow(IllegalArgumentException.class);
        when(parkingSpotDAO.getNextAvailableSlot(any(ParkingType.class))).thenReturn(1);

        final ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);
        //WHEN
        parkingService.processIncomingVehicle();
        //ASSERT
        verify(displayService, Mockito.times(2)).display(messageCaptor.capture());
        assertThat(messageCaptor.getAllValues())
                .contains("Incorrect vehicle number");

    }

    @ParameterizedTest
    @DisplayName("When a vehicle spot of type {0} is not available")
    @EnumSource(ParkingType.class)
    public void park_whenNoSpotAvailable_shouldDisplayNoSpotMessage(ParkingType parkingType) {

        lenient().when(inputReaderUtil.readSelection()).thenReturn(parkingType.ordinal()+1);
        when(parkingSpotDAO.getNextAvailableSlot(parkingType)).thenThrow(new NoSpotAvailableException(parkingType));

        final ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);
        //WHEN
        parkingService.processIncomingVehicle();

        verify(displayService, Mockito.times(1)).display(messageCaptor.capture());
        assertThat(messageCaptor.getAllValues())
                .contains("Sorry do not have place for your " + parkingType.name());
    }

    @Test
    @DisplayName("When a vehicle incoming with wrong input type ")
    public void park_whenWrongInputType_shouldDisplayMessageIncorrectEntry() {
        //GET
        when(inputReaderUtil.readSelection()).thenReturn(0);
        final ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);
        //WHEN
        parkingService.processIncomingVehicle();
        //ASSERT
        verify(displayService, Mockito.times(1)).display(messageCaptor.capture());
        assertThat(messageCaptor.getAllValues())
                .contains("Vehicle type not found");

    }


    @ParameterizedTest
    @EnumSource(ParkingType.class)
    @DisplayName("When a {0} exiting the parking")
    public void park_whenVehicleExiting_shouldReturnNoVehicle(ParkingType parkingType)
    {
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenReturn("AAA");
        Ticket ticket = Ticket.builder()
                .inTime(LocalDateTime.now().minusHours(1))
                .vehicleRegNumber("AAA")
                .id(1)
                .parkingSpot(new ParkingSpot(1,parkingType,false))
                .recurrentFee(true)
                .build();
        when(ticketDAO.getTicketToPaid(anyString())).thenReturn(ticket);
        when(ticketDAO.updateTicket(ticket)).thenReturn(true);

        parkingService.processExitingVehicle();

        assertThat(ticket)
                .extracting(Ticket::getParkingSpot)
                .extracting(ParkingSpot::isAvailable).isEqualTo(true);
        verify(ticketDAO,times(1)).updateTicket(any());
        verify(ticketDAO,times(1)).getTicketToPaid(any());
        verify(parkingSpotDAO,times(1)).updateParking(any());
    }

    @ParameterizedTest
    @EnumSource(ParkingType.class)
    @DisplayName("When a {0} exiting the parking and the update failed")
    public void park_whenVehicleExitingAndUpdateFailed_shouldDisplayWarningMessage(ParkingType parkingType)
    {
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenReturn("AAA");
        Ticket ticket = Ticket.builder()
                .inTime(LocalDateTime.now().minusHours(1))
                .vehicleRegNumber("AAA")
                .id(1)
                .parkingSpot(new ParkingSpot(1,parkingType,false))
                .recurrentFee(true)
                .build();
        when(ticketDAO.getTicketToPaid(anyString())).thenReturn(ticket);
        when(ticketDAO.updateTicket(ticket)).thenReturn(false);

        parkingService.processExitingVehicle();

        verify(displayService,times(2)).display(any());

    }

    @Test
    @DisplayName("When exiting the parking and the ticket is null")
    public void park_whenVehicleExitingWithNoTicket_shouldDisplayWarningMessage()
    {
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenReturn("AAA");
        when(ticketDAO.getTicketToPaid(anyString())).thenReturn(null);

        parkingService.processExitingVehicle();

        verify(displayService,times(2)).display(any());

    }

    @Test
    @DisplayName("When exiting the parking and the input user throw an exception")
    public void park_whenVehicleExitingWithWrongInput_shouldDisplayWarningMessage()
    {
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenThrow(IllegalArgumentException.class);
        parkingService.processExitingVehicle();

        verify(displayService,times(2)).display(any());

    }

}
