package com.parkit.parkingsystem.integration;

import com.parkit.parkingsystem.dao.impl.ParkingSpotDAO;
import com.parkit.parkingsystem.dao.impl.TicketDAO;
import com.parkit.parkingsystem.integration.config.DataBaseTestConfig;
import com.parkit.parkingsystem.integration.service.DataBasePrepareService;
import com.parkit.parkingsystem.model.ParkingSpot;
import com.parkit.parkingsystem.model.Ticket;
import com.parkit.parkingsystem.service.FareCalculatorService;
import com.parkit.parkingsystem.service.impl.RecurrentFeeCalculator;
import com.parkit.parkingsystem.service.ParkingService;
import com.parkit.parkingsystem.service.impl.ConsoleDisplayService;
import com.parkit.parkingsystem.service.contract.DisplayService;
import com.parkit.parkingsystem.util.DatabasePropertiesLoader;
import com.parkit.parkingsystem.util.InputReaderUtil;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ParkingServiceSIT {

    private static DataBaseTestConfig dataBaseTestConfig;
    private static ParkingSpotDAO parkingSpotDAO;
    private static TicketDAO ticketDAO;
    private static DataBasePrepareService dataBasePrepareService;
    private static FareCalculatorService fareCalculatorService;
    private static DisplayService displayService;

    @Mock
    private static InputReaderUtil inputReaderUtil;

    @BeforeAll
    private static void setUp() throws Exception {
        fareCalculatorService = new FareCalculatorService(new RecurrentFeeCalculator());
        DatabasePropertiesLoader databasePropertiesLoader = new DatabasePropertiesLoader();
        Properties dbProperties = databasePropertiesLoader.getDbProperties();
        dataBaseTestConfig = new DataBaseTestConfig(dbProperties);
        dataBasePrepareService = new DataBasePrepareService(dataBaseTestConfig);
        parkingSpotDAO = new ParkingSpotDAO(dataBaseTestConfig);
        ticketDAO = new TicketDAO(dataBaseTestConfig);

        displayService = new ConsoleDisplayService();

    }

    @BeforeEach
    private void setUpPerTest()  {

        dataBasePrepareService.clearDataBaseEntries();
    }

    @AfterAll
    private static void tearDown() {
        dataBasePrepareService = null;
        parkingSpotDAO = null;
        ticketDAO = null;
        dataBaseTestConfig = null;
    }

    @Test
    @DisplayName("Save a vehicle entry in parking ")
    public void parkingCar_whenCarPark_shouldBeRegistred() {
        //WHEN
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenReturn("ABCDEF");
        when(inputReaderUtil.readSelection()).thenReturn(1);
        ParkingService parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO,fareCalculatorService,displayService);

        parkingService.processIncomingVehicle();


        //ASSERT
        Ticket ticket = ticketDAO.getLastTicket("ABCDEF");
        assertThat(ticket)
                .extracting(Ticket::getParkingSpot)
                .extracting(ParkingSpot::isAvailable)
                .isEqualTo(false);

    }

    @Test
    @DisplayName("Save a vehicle left the parking ")
    public void leftParking_whenVehicleLeaveTheParking_shouldCalculatePriceAndFreeParkingPlace(){
        //GET
        when(inputReaderUtil.readVehicleRegistrationNumber()).thenReturn("AAAAA");
        ParkingService parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO,fareCalculatorService,displayService);

       //WHEN
        parkingService.processExitingVehicle();

        Ticket myTicket = ticketDAO.getLastTicket("AAAAA");
        //ASSERT
        assertThat(myTicket.getPrice()).isEqualTo(3.0);
        assertThat(myTicket.getOutTime()).isAfter(myTicket.getInTime());
        assertThat(myTicket.getParkingSpot().isAvailable()).isEqualTo(true);
    }


}
