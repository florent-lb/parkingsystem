package com.parkit.parkingsystem.service.contract;

public interface FeeCalculator {

    /**
     * The detail of the calc like rounding can change for each implementation see implemented method
     * @param initialPrice the price to impose
     * @return the price imposed
     * @throw IllegalArgumentException if the price is not parsable as a double
     */
    Double calculate(Double initialPrice) throws IllegalArgumentException;

    /**
     * Return the fee rate
     * @return the fee rate
     */
    double getFeeRate();
}
