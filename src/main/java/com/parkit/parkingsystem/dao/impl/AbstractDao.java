package com.parkit.parkingsystem.dao.impl;

import com.parkit.parkingsystem.config.DataBaseConfig;
import com.parkit.parkingsystem.dao.contract.Dao;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Abstract class for the DAOs
 * Implements methods for closing/opening and prepare JDBC statements
 */
@Log4j2(topic = "AbstractDao")
@RequiredArgsConstructor
abstract class AbstractDao implements Dao {

    protected final DataBaseConfig dataBaseConfig;

    private Connection connection;


    @Override
    public Optional<ResultSet> executeJdbcQuery(String query, List<Object> orderedArguments) {

        return getConnection().map(con ->
        {
            try {
                connection = con;
                PreparedStatement preparedStatement = con.prepareStatement(query);
                prepareArgumentsQuery(preparedStatement, orderedArguments);
                 return preparedStatement.executeQuery();
            } catch (SQLException e) {
                logger.warn("Impossible to query BDD", e);
                return null;
            }
        });
    }

    /**
     * Apply the argument to the JDB query
     * @param preparedStatement The prepared statement to set
     * @param orderedArguments the list of arguments in order of the query
     * @throws SQLException if the type of the argument is not implemented
     */
    protected void prepareArgumentsQuery(PreparedStatement preparedStatement, List<Object> orderedArguments) throws SQLException {
        int indexJDBC = 1;
        for (Object parameter : orderedArguments)
        {
            if (parameter instanceof String) {
                preparedStatement.setString(indexJDBC, parameter.toString());
            } else if (parameter instanceof Integer) {
                preparedStatement.setInt(indexJDBC, Integer.parseInt(parameter.toString()));
            } else if (parameter instanceof Boolean) {
                preparedStatement.setBoolean(indexJDBC, Boolean.parseBoolean(parameter.toString()));
            } else if (parameter instanceof Double) {
                preparedStatement.setDouble(indexJDBC, Double.parseDouble(parameter.toString()));
            } else if (parameter instanceof LocalDateTime) {
                LocalDateTime dateTime = (LocalDateTime) parameter;
                preparedStatement.setTimestamp(indexJDBC, Timestamp.valueOf(dateTime));
            } else if (parameter == null) {
                preparedStatement.setObject(indexJDBC, null);

            } else {
                throw new SQLException("Type of object : " + parameter.getClass().getName() + " have no parser, implement it !");
            }
            indexJDBC++;
        }
    }



    @Override
    public boolean executeUpdateJdbcQuery(String query, List<Object> orderedArguments) {
        return getConnection().map(con ->
        {
            try {
                connection = con;
                PreparedStatement preparedStatement = con.prepareStatement(query);
                prepareArgumentsQuery(preparedStatement, orderedArguments);
                return preparedStatement.executeUpdate() == 1;
            } catch (SQLException e) {
                logger.warn("Impossible to query BDD", e);
                return false;
            }
        }).orElse(false);
    }

    /**
     * Secure getter for have an SQL connection
     * @return a potential connection
     */
    public Optional<Connection> getConnection() {

        try {
            if(connection == null) {
                return Optional.of(dataBaseConfig.getConnection());
            }
            else
            {
                return Optional.of(connection);
            }
        } catch (ClassNotFoundException | SQLException e) {
            logger.warn("Impossible to get the connection", e);
            return Optional.empty();
        }
    }

    /**
     * Call it for close the actual connection
     */
    protected void closeConnection()
    {
        dataBaseConfig.closeConnection(connection);
        connection = null;
    }
}
