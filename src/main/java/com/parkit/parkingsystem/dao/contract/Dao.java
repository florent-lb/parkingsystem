package com.parkit.parkingsystem.dao.contract;

import java.sql.ResultSet;
import java.util.List;
import java.util.Optional;

/**
 * Interface for JDBC Dao
 */
public interface Dao {

    /**
     * Method for executing a query with a result set
     * @param preparedStatement the query to execute
     * @param orderedArguments Ordered arguments
     * @return the ResultSet from the query if is present
     */
    Optional<ResultSet> executeJdbcQuery(String preparedStatement, List<Object> orderedArguments);
    /**
     * Method for executing a execute/update query who apply on only one line
     * @param preparedStatement the query to execute
     * @param orderedArguments the list of arguments in order of the query
     * @return true if only one line is updated/deleted
     */
    boolean executeUpdateJdbcQuery(String preparedStatement, List<Object> orderedArguments);
}
