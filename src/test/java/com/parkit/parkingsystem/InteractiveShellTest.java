package com.parkit.parkingsystem;

import com.parkit.parkingsystem.config.DataBaseConfig;
import com.parkit.parkingsystem.service.InteractiveShell;
import com.parkit.parkingsystem.service.contract.DisplayService;
import com.parkit.parkingsystem.util.InputReaderUtil;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class InteractiveShellTest {

    private InteractiveShell shell;

    @Mock
    private InputReaderUtil inputReaderUtil;

    @Mock
    private DisplayService displayService;

    @Mock
    private DataBaseConfig dbConfig;

    @ParameterizedTest
    @ValueSource(ints = {1,2})
    @DisplayName("When user choose {0} then 3 to exit")
    public void choose_whenUserChooseOne_shouldReturnNextMenu(Integer chooseValue) {
        shell = new InteractiveShell(inputReaderUtil, displayService,dbConfig);

        when(inputReaderUtil.readSelection()).thenReturn(chooseValue, 3);
        shell.loadInterface();
        verify(displayService, atLeast(4)).display(any());

    }

}
