package com.parkit.parkingsystem.service.contract;

/**
 * Entry point for display a message for a user
 */
public interface DisplayService
{
    /**
     * Method to call for display the message
     * @param toDisplay message to display
     */
    void display(String toDisplay);
}
