package com.parkit.parkingsystem.model;

import com.parkit.parkingsystem.constants.ParkingType;
import lombok.*;
/**
 * Entity representing a parking spot
 */
@Getter
@AllArgsConstructor
@ToString
public class ParkingSpot {

    /**
     * identifier of a parking
     */
    private final int number;

    /**
     * The type of the parking
     */
    private ParkingType parkingType;

    /**
     * true if the parking is available
     */
    @Setter
    private boolean isAvailable;


}
