package com.parkit.parkingsystem.dao.impl;

import com.parkit.parkingsystem.config.DataBaseConfig;
import com.parkit.parkingsystem.constants.DBConstants;
import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.model.ParkingSpot;
import com.parkit.parkingsystem.model.Ticket;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

/**
 * DAO for the ticket
 */
@Log4j2(topic = "TicketDAO")

public class TicketDAO extends AbstractDao {

    public TicketDAO(DataBaseConfig dataBaseConfig) {
        super(dataBaseConfig);
    }

    /**
     * Use for save a new ticket in bdd
     *
     * @param ticket the ticket to create
     * @return true when the ticket is persisted
     */
    public boolean saveTicket(Ticket ticket) {

        List<Object> arguments = new ArrayList<>();
        arguments.add(ticket.getParkingSpot().getNumber());
        arguments.add(ticket.getVehicleRegNumber());
        arguments.add(ticket.getPrice());
        arguments.add(ticket.getInTime());
        if (Optional.ofNullable(ticket.getOutTime()).isPresent()) {
            arguments.add(ticket.getOutTime());
        } else {
            arguments.add(null);
        }
        logger.debug("Ticker to update : " + ticket.toString());
        closeConnection();
        return executeUpdateJdbcQuery(DBConstants.SAVE_TICKET, arguments);
    }

    /**
     * Get the last ticket for the user in parameter
     *
     * @param vehicleRegNumber identifier of the user
     * @return the ticket found
     */
    public Ticket getLastTicket(String vehicleRegNumber) {

        return getTicket(DBConstants.GET_LAST_TICKET, vehicleRegNumber);
    }

    /**
     * Get the last ticket to paid for the user in parameter
     *
     * @param vehicleRegNumber identifier of the user
     * @return the ticket found
     */
    public Ticket getTicketToPaid(String vehicleRegNumber) {
        return getTicket(DBConstants.GET_TICKET_TO_PAID, vehicleRegNumber);

    }

    /**
     * Prepare a get method on ticket table
     *
     * @param query            the query used for get the ticket see {@link DBConstants}
     * @param vehicleRegNumber the number of the vehicle
     * @return the last ticket to paid (with out time null)
     */
    private Ticket getTicket(String query, String vehicleRegNumber) {
        AtomicReference<Ticket> atoTicket = new AtomicReference<>();
        List<Object> arguments = new ArrayList<>();
        arguments.add(vehicleRegNumber);
        executeJdbcQuery(query, arguments)
                .ifPresent(resultSet -> {
                    try {
                        if (resultSet.next()) {
                            Ticket ticket = new Ticket();
                            ParkingSpot parkingSpot = new ParkingSpot(resultSet.getInt(1), ParkingType.valueOf(resultSet.getString(6)), resultSet.getBoolean(7));
                            ticket.setParkingSpot(parkingSpot);
                            ticket.setId(resultSet.getInt(2));
                            ticket.setVehicleRegNumber(vehicleRegNumber);
                            ticket.setPrice(resultSet.getDouble(3));
                            ticket.setInTime(resultSet.getTimestamp(4).toLocalDateTime());
                            ticket.setOutTime(Optional.ofNullable(resultSet.getTimestamp(5)).map(Timestamp::toLocalDateTime).orElse(null));
                            atoTicket.set(ticket);
                        }
                    } catch (SQLException e) {
                        logger.error("Error fetching ticket", e);
                    }
                });
        closeConnection();

        return atoTicket.get();
    }


    /**
     * Update method for a ticket
     *
     * @param ticket the new ticket to update
     * @return true when the ticket is updated
     */
    public boolean updateTicket(Ticket ticket) {

        try {

            List<Object> arguments = new ArrayList<>();
            arguments.add(ticket.getPrice());
            arguments.add(ticket.getOutTime());
            arguments.add(ticket.getId());
            logger.debug("Ticket to update : " + ticket.toString());
            return executeUpdateJdbcQuery(DBConstants.UPDATE_TICKET, arguments);
        } catch (Exception ex) {
            logger.error("Error saving ticket info", ex);
        } finally {
            closeConnection();
        }
        return false;
    }

    /**
     * Call it for know if the user have already use the parking
     *
     * @param vehicleRegNumber the identifier of the vehicle
     * @return true if the user is actually a recurrent user
     */
    public boolean isRecurrentUser(String vehicleRegNumber) {
        Connection con = null;
        try {

            List<Object> arguments = new ArrayList<>();
            arguments.add(vehicleRegNumber);

            return executeJdbcQuery(DBConstants.IS_RECURRENT_USER, arguments).
                    map(resultSet ->
                            {
                                try {
                                    if (resultSet.next()) {
                                        return resultSet.getInt(1) > 0;
                                    } else {
                                        return false;
                                    }
                                } catch (SQLException e) {
                                    logger.error("Impossible to connect BDD", e);
                                    return false;
                                }
                            }
                    ).orElse(false);

        } catch (Exception ex) {
            logger.error("Impossible to fetch if the user is a recurrent user or not", ex);
        }
        closeConnection();
        return false;
    }
}
