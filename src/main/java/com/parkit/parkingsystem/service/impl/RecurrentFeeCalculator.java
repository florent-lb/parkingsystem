package com.parkit.parkingsystem.service.impl;

import com.parkit.parkingsystem.service.contract.FeeCalculator;
import lombok.extern.log4j.Log4j2;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;

/**
 * Fee calculator for the recurrent user
 */
@Log4j2(topic = "RecurrentFeeCalculator")
public class RecurrentFeeCalculator implements FeeCalculator
{
    final public static double FEE_PERCENTAGE_RECURRENT_USER = 5.0d;

    @Override
    /**
     * {@inheritDoc}
     * For this calc the rounding mode is set to floor, and the price is return with two digit maximum after the dot.
     */
    public Double calculate(Double price) throws IllegalArgumentException {
        try {

            DecimalFormat decimalFormat = new DecimalFormat("#.##");
            decimalFormat.setRoundingMode(RoundingMode.FLOOR);
            return decimalFormat.parse(decimalFormat.format(price * (1.0-FEE_PERCENTAGE_RECURRENT_USER/100))).doubleValue();

        } catch (ParseException ex) {
            logger.error("Impossible to calculate price with 5% fee ", ex);
            throw new IllegalArgumentException("Impossible to calculate price with 5% fee");
        }
    }

    @Override
    public double getFeeRate() {
        return FEE_PERCENTAGE_RECURRENT_USER;
    }
}
