package com.parkit.parkingsystem;

import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.exception.VehicleAlwaysInException;
import com.parkit.parkingsystem.model.ParkingSpot;
import com.parkit.parkingsystem.model.Ticket;
import com.parkit.parkingsystem.service.FareCalculatorService;
import com.parkit.parkingsystem.service.impl.RecurrentFeeCalculator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.time.LocalDateTime;

import static com.parkit.parkingsystem.constants.ParkingType.BIKE;
import static com.parkit.parkingsystem.constants.ParkingType.CAR;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FareCalculatorServiceTest {

    private FareCalculatorService fareCalculatorService;
    private Ticket ticket;
    private LocalDateTime outTime;

    @Mock
    private RecurrentFeeCalculator recurrentFeeCalculator;

    @BeforeEach
    private void setUpPerTest() {
        fareCalculatorService = new FareCalculatorService(recurrentFeeCalculator);
        ticket = new Ticket();
        ticket.setInTime(LocalDateTime.now());
        outTime = LocalDateTime.now();
        ticket.setVehicleRegNumber("ABCDEF");
    }

    @ParameterizedTest
    @EnumSource(ParkingType.class)
    @DisplayName("Calculate one hour fare for {0}")
    public void calculateFareBike(ParkingType parkingType) {
        int nbHour = 1;
        outTime = outTime.plusHours(nbHour);
        ParkingSpot parkingSpot = new ParkingSpot(1, parkingType, false);


        ticket.setOutTime(outTime);
        ticket.setParkingSpot(parkingSpot);
        fareCalculatorService.calculateFare(ticket);
        assertThat((ticket.getPrice()))
                .isEqualTo(parkingType.getFare() * nbHour);
    }

    @Test
    @DisplayName("Calculate when type vehicle is unknown")
    public void calculateFareUnkownType() {
        outTime = outTime.plusHours(1);
        ParkingSpot parkingSpot = new ParkingSpot(1, null, false);


        ticket.setOutTime(outTime);
        ticket.setParkingSpot(parkingSpot);

        Throwable thrown = catchThrowable(() -> fareCalculatorService.calculateFare(ticket));
        assertThat(thrown).isInstanceOf(NullPointerException.class);
    }

    @Test
    @DisplayName("Calculate fare when out time is before in time")
    public void calculateFareBikeWithFutureInTime() {
        outTime = outTime.minusHours(1);
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.BIKE, false);


        ticket.setOutTime(outTime);
        ticket.setParkingSpot(parkingSpot);

        Throwable thrown = catchThrowable(() -> fareCalculatorService.calculateFare(ticket));
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    @DisplayName("The bike stay less than one hour but more 30 minutes")
    public void calculateFareBikeWithLessThanOneHourParkingTime() {
        outTime = outTime.plusMinutes(45);
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.BIKE, false);


        ticket.setOutTime(outTime);
        ticket.setParkingSpot(parkingSpot);
        fareCalculatorService.calculateFare(ticket);
        assertThat(ticket.getPrice())
                .isEqualTo(0.75 * BIKE.getFare());
    }

    @ParameterizedTest
    @EnumSource(ParkingType.class)
    @DisplayName("The {0} stay less than one hour but more 30 minutes")
    public void calculateFareCarWithLessThanOneHourParkingTime(ParkingType parkingType) {
        outTime = outTime.plusMinutes(45);
        ParkingSpot parkingSpot = new ParkingSpot(1, parkingType, false);

        ticket.setOutTime(outTime);
        ticket.setParkingSpot(parkingSpot);
        fareCalculatorService.calculateFare(ticket);
        assertThat(ticket.getPrice())
                .isEqualTo(0.75 * parkingType.getFare());
    }

    @Test
    @DisplayName("The car is always inside")
    public void calculateFareCar_whenOutTimeNull_shouldThrowVehiculeAlwaysInException() {
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.CAR, false);

        ticket.setOutTime(null);
        ticket.setParkingSpot(parkingSpot);
        Throwable thrown = catchThrowable(() -> fareCalculatorService.calculateFare(ticket));

        assertThat(thrown).isInstanceOf(VehicleAlwaysInException.class);

    }

    @Test
    @DisplayName("The car stay less than 30 minutes, it's free")
    public void calculateFareCar_whenCarStayLessThan30minutes_shouldReturnZeroFee() {
        outTime = outTime.plusMinutes(20);
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.CAR, false);


        ticket.setOutTime(outTime);
        ticket.setParkingSpot(parkingSpot);
        fareCalculatorService.calculateFare(ticket);
        assertThat(ticket.getPrice())
                .isEqualTo(0);


    }

    @Test
    @DisplayName("Recurrent user get 5% free")
    public void calculateFare_whenRecurrentUserIncomming_shouldCallOnceFeeCalculator() throws ParseException {
        //GET
        outTime = outTime.plusHours(2);
        ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.CAR, false);
        ticket.setParkingSpot(parkingSpot);
        ticket.setOutTime(outTime);
        ticket.setRecurrentFee(true);

        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        decimalFormat.setRoundingMode(RoundingMode.FLOOR);
        Double calcSolution = decimalFormat.parse(decimalFormat.format((2* CAR.getFare()) * 0.95)).doubleValue();

        when(recurrentFeeCalculator.calculate(2* CAR.getFare())).thenReturn(calcSolution);

        final ArgumentCaptor<Double> feeCalculatorServiceCaptor =
                ArgumentCaptor.forClass(Double.class);



        fareCalculatorService.calculateFare(ticket);
        //WHEN
        verify(recurrentFeeCalculator,times(1))
                .calculate(feeCalculatorServiceCaptor.capture());


        assertThat(ticket.getPrice())
                .isEqualTo(calcSolution);

    }


}
