package com.parkit.parkingsystem.integration;

import com.parkit.parkingsystem.dao.impl.TicketDAO;
import com.parkit.parkingsystem.integration.config.DataBaseTestConfig;
import com.parkit.parkingsystem.integration.service.DataBasePrepareService;
import com.parkit.parkingsystem.model.Ticket;
import com.parkit.parkingsystem.util.DatabasePropertiesLoader;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class TicketDaoTestIT
{

    private static TicketDAO ticketDAO;


    @BeforeAll
    public static void setup() throws IOException {
        DatabasePropertiesLoader databasePropertiesLoader = new DatabasePropertiesLoader();
        DataBaseTestConfig dataBaseTestConfig = new DataBaseTestConfig(databasePropertiesLoader.getDbProperties());
        ticketDAO = new TicketDAO(dataBaseTestConfig);
        DataBasePrepareService  dataBasePrepareService = new DataBasePrepareService(dataBaseTestConfig);
        dataBasePrepareService.clearDataBaseEntries();
    }


    @Test
    @DisplayName("Testing get ticket to paid ")
    public void getTicket_whenGetTicketToPaid_ShouldReturnTicket()
    {
       Ticket ticketToPaid =  ticketDAO.getTicketToPaid("AAAAA");
       Assertions.assertThat(ticketToPaid.getOutTime()).isNull();

    }
    @Test
    @DisplayName("Testing get ticket to paid - no ticket found ")
    public void getTicket_whenGetTicketDoNotExist_ShouldReturnNull()
    {
        Ticket ticketToPaid =  ticketDAO.getTicketToPaid("AA");
        Assertions.assertThat(ticketToPaid).isNull();

    }

}
