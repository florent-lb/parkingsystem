package com.parkit.parkingsystem.service;

import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.exception.VehicleAlwaysInException;
import com.parkit.parkingsystem.model.Ticket;
import com.parkit.parkingsystem.service.contract.FeeCalculator;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.time.Duration;

/**
 * Service for calculate the Fare and apply fee
 */
@Log4j2(topic = "FareCalculatorService")
@RequiredArgsConstructor
public class FareCalculatorService {

    public final static int NB_MINUTES_FREE = 30;

    private final FeeCalculator recurrentFeeCalculator;

    /**
     * Method for calc the fare with a ticket
     * @param ticket the ticket to calc the price
     */
    public void calculateFare(Ticket ticket)
    {
        if ((ticket.getOutTime() == null)) {
            throw new VehicleAlwaysInException(ticket.getVehicleRegNumber());
        } else if (ticket.getOutTime().isBefore(ticket.getInTime())) {
            throw new IllegalArgumentException("Out time provided is incorrect : " + ticket.getOutTime().toString());
        }
        Duration duration = Duration.between(ticket.getInTime(), ticket.getOutTime());
        double durationRatio = duration.toHours();
        //If the car stay less than 1 Hour
        if (durationRatio == 0) {
            //For creation a ratio from minutes to base 1
            durationRatio = (duration.toMinutes()) / 60.0d;
            //the 30nd minutes are free
            if (duration.toMinutes() <= NB_MINUTES_FREE) {
                durationRatio = 0;
            }
        }
        switch (ticket.getParkingSpot().getParkingType()) {
            case CAR: {
                ticket.setPrice(durationRatio * ParkingType.CAR.getFare());
                break;
            }
            case BIKE: {
                ticket.setPrice(durationRatio * ParkingType.BIKE.getFare());
                break;
            }
        }

        if (ticket.isRecurrentFee())
        {
            ticket.setPrice(recurrentFeeCalculator.calculate(ticket.getPrice()));
        }

    }


}



