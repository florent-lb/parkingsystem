package com.parkit.parkingsystem.dao.impl;

import com.parkit.parkingsystem.config.DataBaseConfig;
import com.parkit.parkingsystem.constants.DBConstants;
import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.exception.NoSpotAvailableException;
import com.parkit.parkingsystem.model.ParkingSpot;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Dao for the parking table
 */
@Log4j2(topic = "ParkingSpotDAO")
public class ParkingSpotDAO extends AbstractDao {

    public ParkingSpotDAO(DataBaseConfig dataBaseConfig) {
        super(dataBaseConfig);
    }

    /**
     * get the next available parking spot number for a vehicle type
     * @param parkingType The type of parking
     * @return the number of the available parking
     * @throws NoSpotAvailableException if no spot is available for this type of vehicle
     */
    public Integer getNextAvailableSlot(ParkingType parkingType) throws NoSpotAvailableException {
        Objects.requireNonNull(parkingType, "You must choose a parking type before query!");
        List<Object> arguments = new ArrayList<>();
        arguments.add(parkingType.name());

        try {
            ResultSet rs = executeJdbcQuery(DBConstants.GET_NEXT_PARKING_SPOT, arguments)
                    .orElseThrow(SQLException::new);
            if (rs.next()) {
                int retour = rs.getInt(1);
                if (retour > 0)
                {
                    return retour;
                }
                else {
                    throw new NoSpotAvailableException(parkingType);
                }
            } else {
                throw new NoSpotAvailableException(parkingType);
            }
        } catch (SQLException ex) {
            logger.error("Error fetching next available spot", ex);
            return null;
        } finally {
            closeConnection();
        }


    }

    /**
     * Update the parking spot in parameter
     * @param parkingSpot the parking spot to update
     * @return true when the parking is updated
     */
    public boolean updateParking(ParkingSpot parkingSpot) {
        //update the availability fo that parking slot
        Connection con = null;
        try {
            con = dataBaseConfig.getConnection();
            PreparedStatement ps = con.prepareStatement(DBConstants.UPDATE_PARKING_SPOT);
            ps.setBoolean(1, parkingSpot.isAvailable());
            ps.setInt(2, parkingSpot.getNumber());
            int updateRowCount = ps.executeUpdate();
            dataBaseConfig.closePreparedStatement(ps);
            return (updateRowCount == 1);
        } catch (Exception ex) {
            logger.error("Error updating parking info", ex);
            return false;
        } finally {
            dataBaseConfig.closeConnection(con);
        }
    }

}
