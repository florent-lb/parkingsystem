package com.parkit.parkingsystem.model;

import lombok.*;

import java.time.LocalDateTime;

/**
 * Object represent the parking ticket with price, date and user informations.
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Ticket {
    /**
     * Technical id, auto-increment in BDD
     */
    private int id;
    /**
     * Spot reserved for this ticket
     */
    private ParkingSpot parkingSpot;
    /**
     * Vehicle Number registred
     */
    private String vehicleRegNumber;
    /**
     * Price to paid when user out, must be at 0.0 when user in the parking
     */
    private double price;
    /**
     * Date of incomming user in parking
     */
    private LocalDateTime inTime;
    /**
     * Date set when the user outcomming, must be null at the incomming
     */
    private LocalDateTime outTime;
    /**
     * True if the user have a fee
     */
    private boolean recurrentFee;



}
