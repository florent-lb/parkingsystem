package com.parkit.parkingsystem.dao;

import com.parkit.parkingsystem.config.DataBaseConfig;
import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.dao.impl.TicketDAO;
import com.parkit.parkingsystem.model.ParkingSpot;
import com.parkit.parkingsystem.model.Ticket;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TicketDaoTest {

    private TicketDAO ticketDAO;
    private Ticket ticket;

    @Mock
    private DataBaseConfig dataBaseConfig;

    @Mock
    private Connection con;

    @Mock
    private PreparedStatement preparedStatement;

    @Mock
    private ResultSet resultSet;


    @BeforeEach
    public void setup() throws SQLException, ClassNotFoundException {
        ticketDAO = new TicketDAO(dataBaseConfig);

        ticket = new Ticket();
        ticket.setParkingSpot(new ParkingSpot(1, ParkingType.CAR, true));
        ticket.setPrice(0);
        ticket.setRecurrentFee(true);
        ticket.setOutTime(null);
        ticket.setInTime(LocalDateTime.now());
        ticket.setVehicleRegNumber("AAA");

        when(dataBaseConfig.getConnection()).thenReturn(con);
        when(con.prepareStatement(anyString())).thenReturn(preparedStatement);
    }


    @Test
    @DisplayName("When save a ticket in")
    public void saveTicket_whenSaveTicket_shouldReturnTrue() throws SQLException {
        when(preparedStatement.executeUpdate()).thenReturn(1);

        boolean isExecuted = ticketDAO.saveTicket(ticket);

        Assertions.assertThat(isExecuted).isEqualTo(true);
    }

    @Test
    @DisplayName("When save a ticket and failed")
    public void saveTicket_whenSaveTicketAndUpdateFaild_shouldReturnFase() throws SQLException {
        when(preparedStatement.executeUpdate()).thenReturn(0);
        ticket.setOutTime(LocalDateTime.now());

        boolean isExecuted = ticketDAO.saveTicket(ticket);

        Assertions.assertThat(isExecuted).isEqualTo(false);
    }

    @Test
    @DisplayName("When save a ticket and something wrong happen")
    public void saveTicket_whenSaveTicketAndSomthingWrong_shouldReturnFalse() throws SQLException {
        when(preparedStatement.executeUpdate()).thenThrow(SQLException.class);

        boolean isExecuted = ticketDAO.saveTicket(ticket);

        Assertions.assertThat(isExecuted).isEqualTo(false);
    }

    @Test
    @DisplayName("When get a ticket and no ticket found")
    public void geticket_whenGetATicketAndNoTicketFound_shouldReturnNull() throws SQLException {

        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(false);

        Ticket myTicket = ticketDAO.getLastTicket("AAA");

        Assertions.assertThat(myTicket)
                .isNull();
    }


    @Test
    @DisplayName("When get a ticket and ResultSet throw an error")
    public void geticket_whenGetATicketAndResultSetThrowSQLException_shouldReturnNull() throws SQLException {

        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenThrow(SQLException.class);

        Ticket myTicket = ticketDAO.getLastTicket("AAA");

        Assertions.assertThat(myTicket)
                .isNull();
    }

    @Test
    @DisplayName("When update a ticket request failed ")
    public void updateticket_whenUpdateATicketAndUpdateFailed_shouldReturnFalse() throws SQLException {

        when(preparedStatement.executeUpdate()).thenThrow(SQLException.class);

        Boolean isUpdated = ticketDAO.updateTicket(ticket);

        Assertions.assertThat(isUpdated)
                .isEqualTo(false);
    }

    @Test
    @DisplayName("Check if is a recurrent user")
    public void isrecurrentUser_whenAskifUserIsRecurrentAndFailed_shouldReturnFalse() throws SQLException{


        when(preparedStatement.executeQuery()).thenThrow(SQLException.class);

        Boolean isRecurrent = ticketDAO.isRecurrentUser("AAA");

        Assertions.assertThat(isRecurrent)
                .isEqualTo(false);
    }
    @Test
    @DisplayName("Check if is a recurrent user but is not")
    public void isrecurrentUser_whenAskifUserIsRecurrentAndisNot_shouldReturnFalse() throws SQLException{


        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(false);

        Boolean isRecurrent = ticketDAO.isRecurrentUser("AAA");

        Assertions.assertThat(isRecurrent)
                .isEqualTo(false);
    }
    @Test
    @DisplayName("Check if is a recurrent user but resultSet failed")
    public void isrecurrentUser_whenAskifUserIsRecurrentButResultSetFailed_shouldReturnFalse() throws SQLException{


        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenThrow(SQLException.class);

        Boolean isRecurrent = ticketDAO.isRecurrentUser("AAA");

        Assertions.assertThat(isRecurrent)
                .isEqualTo(false);
    }


}
