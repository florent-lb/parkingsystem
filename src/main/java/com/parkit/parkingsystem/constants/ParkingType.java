package com.parkit.parkingsystem.constants;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Parking type available with the base per hour fare rate
 */
@RequiredArgsConstructor
public enum ParkingType {

    BIKE(1.0),
    CAR(1.5);

    @Getter
    private final double fare;
}
