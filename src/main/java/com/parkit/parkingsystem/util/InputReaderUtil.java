package com.parkit.parkingsystem.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;
import java.util.Scanner;

/**
 * Utility Class for input in the console
 */
public class InputReaderUtil {

    private static Scanner scan = new Scanner(System.in, "UTF-8");
    private static final Logger logger = LogManager.getLogger("InputReaderUtil");

    /**
     * Read an integer from the console
     * @return the integer read from the user input
     */
    public int readSelection()  {
        try {
           return Optional.ofNullable(scan.nextLine())
            .map(Integer::parseInt)
            .orElse(-1);

        } catch (NumberFormatException e) {
            logger.error("Error while reading user input from Shell", e);
            return -1;
        }
    }

    /**
     * Read a string from the console
     * @return the register number
     * @throws IllegalArgumentException if the number is null or empty
     */
    public String readVehicleRegistrationNumber() throws IllegalArgumentException {

        String vehicleRegNumber = scan.nextLine();
        if (vehicleRegNumber == null || vehicleRegNumber.trim().length() == 0) {
            throw new IllegalArgumentException("Invalid input provided");
        }
        return vehicleRegNumber;

    }


}
