package com.parkit.parkingsystem.config;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.sql.*;
import java.util.Properties;

/**
 * Database configuration<br/>
 * Must have a properties file provided by {@link com.parkit.parkingsystem.util.DatabasePropertiesLoader}
 * or other but you must have theses properties :
 * <ul>
 *     <li>prod.driver.name</li>
 *     <li>prod.url</li>
 *     <li>prod.username</li>
 *     <li>prod.password</li>
 * </ul>
 */
@Log4j2(topic = "DataBaseConfig")
@RequiredArgsConstructor
public class DataBaseConfig {

    @NonNull
    protected Properties dbProperties;

    /**
     * Call to have a connection from the properties file informations
     *
     * @return a sqlconnection
     * @throws ClassNotFoundException if the Driver is not present in the libraries
     * @throws SQLException           if an error occurred during establishment of the connection
     */
    public Connection getConnection() throws ClassNotFoundException, SQLException {
        logger.info("Create DB connection");
        Class.forName(dbProperties.getProperty("prod.driver.name"));
        return DriverManager.getConnection(
                dbProperties.getProperty("prod.url"),
                dbProperties.getProperty("prod.username"),
                dbProperties.getProperty("prod.password"));
    }

    /**
     * Close the connection in the parameter
     * @param con
     */
    public void closeConnection(Connection con) {
        if (con != null) {
            try {
                con.close();
                logger.info("Closing DB connection");
            } catch (SQLException e) {
                logger.error("Error while closing connection", e);
            }
        }
    }
    /**
     * Close the PreparedStatement in the parameter
     * @param ps
     */
    public void closePreparedStatement(PreparedStatement ps) {
        if (ps != null) {
            try {
                ps.close();
                logger.info("Closing Prepared Statement");
            } catch (SQLException e) {
                logger.error("Error while closing prepared statement", e);
            }
        }
    }
    /**
     * Close the ResultSet in the parameter
     * @param rs
     */
    public void closeResultSet(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
                logger.info("Closing Result Set");
            } catch (SQLException e) {
                logger.error("Error while closing result set", e);
            }
        }
    }
}
