package com.parkit.parkingsystem.dao;

import com.parkit.parkingsystem.config.DataBaseConfig;
import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.dao.impl.ParkingSpotDAO;
import com.parkit.parkingsystem.exception.NoSpotAvailableException;
import com.parkit.parkingsystem.model.ParkingSpot;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ParkingDaoTest {


    private ParkingSpotDAO dao;

    @Mock
    private DataBaseConfig dataBaseConfig;

    @Mock
    private Connection connection;

    @Mock
    private PreparedStatement preparedStatement;

    @Mock
    private ResultSet resultSet;

    @BeforeEach
    public void setup()
    {
        dao = new ParkingSpotDAO(dataBaseConfig);
    }

    @ParameterizedTest
    @EnumSource(ParkingType.class)
    @DisplayName("Query for an available spot for type {0}")
    public void query_whenAParkingTypeAvailble_shouldReturnIdSpot(ParkingType parkingType)
    {
        try {
            when(dataBaseConfig.getConnection()).thenReturn(connection);
            when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
            when(preparedStatement.executeQuery()).thenReturn(resultSet);
            when(resultSet.next()).thenReturn(true);
            when(resultSet.getInt(anyInt())).thenReturn(1);

            int parkingNumber = dao.getNextAvailableSlot(parkingType);

            verify(connection,times(1)).prepareStatement(anyString());
            verify(preparedStatement,times(1)).executeQuery();
            verify(resultSet,times(1)).getInt(anyInt());
            Assertions.assertThat(parkingNumber).isEqualTo(1);

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        //WHEN
        dao.getNextAvailableSlot(parkingType);

    }

    @ParameterizedTest
    @EnumSource(ParkingType.class)
    @DisplayName("Query for an unavailable spot for type {0} ")
    public void query_whenAParkingTypeNotAvailble_shouldRThrowNoSpotAvailable(ParkingType parkingType)
    {
        try {
            when(dataBaseConfig.getConnection()).thenReturn(connection);
            when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
            when(preparedStatement.executeQuery()).thenReturn(resultSet);
            when(resultSet.next()).thenReturn(false);

           Throwable thrown = catchThrowable(() ->  dao.getNextAvailableSlot(parkingType));

            Assertions.assertThat(thrown).isInstanceOf(NoSpotAvailableException.class);

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Query with no connection ")
    public void query_whenNoConnection_shouldBeNull()
    {
        try {
            when(dataBaseConfig.getConnection()).thenThrow(SQLException.class);
           Integer parkingNumber = dao.getNextAvailableSlot(ParkingType.CAR);
            Assertions.assertThat(parkingNumber).isNull();

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Update a parkingSpot")
    public void query_updateParkingSpot_shouldReturnTrue()
    {
        try {
            when(dataBaseConfig.getConnection()).thenReturn(connection);
            when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
            when(preparedStatement.executeUpdate()).thenReturn(1);

            boolean updated = dao.updateParking(new ParkingSpot(1,ParkingType.CAR,true));

            assertThat(updated).isEqualTo(true);

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Update a parkingSpot with error SQL(bad return >1)")
    public void query_updateParkingSpotBDDUpdateStrange_shouldReturnFalse()
    {
        try {
            when(dataBaseConfig.getConnection()).thenReturn(connection);
            when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
            when(preparedStatement.executeUpdate()).thenReturn(10);

            boolean updated = dao.updateParking(new ParkingSpot(1,ParkingType.CAR,true));

            assertThat(updated).isEqualTo(false);

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Update a parkingSpot with SQL error (SQL Exception)")
    public void query_updateParkingSpotNoConnection_shouldReturnFalse()
    {
        try {
            when(dataBaseConfig.getConnection()).thenThrow(SQLException.class);
            boolean updated = dao.updateParking(new ParkingSpot(1,ParkingType.CAR,true));
            assertThat(updated).isEqualTo(false);

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

}
