package com.parkit.parkingsystem.util;

import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Properties;

/**
 * Used for read a properties file in the root of the project/application
 */
@Log4j2(topic = "DatabasePropertiesLoader")
public class DatabasePropertiesLoader {

    /**
     * Read a file named database.properties from the root of the project/application
     * @return {@link Properties} with the properties load from the file
     * @throws IOException if no file found or is unreadable
     */
     public Properties getDbProperties() throws IOException {

         Properties dbProperties = new Properties();
         dbProperties.load(Files.newBufferedReader(Paths.get("database.properties")));
         StringBuilder logProperties = new StringBuilder("Load db properties :\n");
         dbProperties
                 .entrySet()
                 .stream()
                 .sorted(Comparator.comparing(entry -> entry.getKey().toString()))
                 .forEach(entry -> logProperties.append(entry.getKey()).append("=").append(entry.getValue()).append("\n") );
         logger.info(logProperties.toString());
         return dbProperties;
     }
}
