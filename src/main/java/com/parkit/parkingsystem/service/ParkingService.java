package com.parkit.parkingsystem.service;

import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.dao.impl.ParkingSpotDAO;
import com.parkit.parkingsystem.dao.impl.TicketDAO;
import com.parkit.parkingsystem.exception.NoSpotAvailableException;
import com.parkit.parkingsystem.model.ParkingSpot;
import com.parkit.parkingsystem.model.Ticket;
import com.parkit.parkingsystem.service.contract.DisplayService;
import com.parkit.parkingsystem.util.InputReaderUtil;
import lombok.extern.log4j.Log4j2;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

/**
 * Service for register incoming and leaving vehicle
 */
@Log4j2(topic = "ParkingService")
public class ParkingService {

    private FareCalculatorService fareCalculatorService;

    private InputReaderUtil inputReaderUtil;
    private ParkingSpotDAO parkingSpotDAO;
    private TicketDAO ticketDAO;
    private DisplayService displayService;

    public ParkingService(InputReaderUtil inputReaderUtil,
                          ParkingSpotDAO parkingSpotDAO,
                          TicketDAO ticketDAO,
                          FareCalculatorService fareCalculatorService,
                          DisplayService displayService) {
        this.inputReaderUtil = inputReaderUtil;
        this.parkingSpotDAO = parkingSpotDAO;
        this.ticketDAO = ticketDAO;
        this.displayService = displayService;
        this.fareCalculatorService = fareCalculatorService;
    }

    /**
     * Method for registered an incoming vehicle
     */
    public void processIncomingVehicle() {


        Optional.ofNullable(getNextParkingNumberIfAvailable())
                .ifPresent(parkingSpot ->
                        Optional.ofNullable(getVehichleRegNumber())
                                .ifPresent(vehicleRegNumber -> {
                                    //Update parking spot is full
                                    parkingSpot.setAvailable(false);
                                    parkingSpotDAO.updateParking(parkingSpot);
                                    //Print ticket
                                    Ticket ticket = Ticket.builder()
                                            .parkingSpot(parkingSpot)
                                            .vehicleRegNumber(vehicleRegNumber)
                                            .price(0)
                                            .inTime(LocalDateTime.now())
                                            .recurrentFee(ticketDAO.isRecurrentUser(vehicleRegNumber))
                                            .build();
                                    //Update ticket persistence
                                    ticketDAO.saveTicket(ticket);
                                    //Display User ticket is registred
                                    displayTicketRegistredMenu(ticket);
                                }));


    }

    /**
     * Display the register Menu, display where the user must parked and the started time of his entry in the parking
     *
     * @param ticket The ticket created
     */
    private void displayTicketRegistredMenu(Ticket ticket) {
        StringBuilder message = new StringBuilder();
        if (ticket.isRecurrentFee()) {
            message.append("Welcome back! As a recurring user of our parking lot, you'll benefit from a 5% discount.\n");
        }
        message.append("Generated Ticket \n")
                .append("Please park your vehicle in spot number:").append(ticket.getParkingSpot().getNumber()).append("\n")
                .append("Recorded in-time for vehicle number:").append(ticket.getVehicleRegNumber()).append(" is:").append(ticket.getInTime());
        displayService.display(message.toString());
    }

    /**
     * Method for read the incoming vehicle number
     *
     * @return Vehicle number
     */
    private String getVehichleRegNumber() {
        displayService.display("Please type the vehicle registration number and press enter key");
        try {
            return inputReaderUtil.readVehicleRegistrationNumber();
        } catch (IllegalArgumentException ex) {
            logger.warn("Incorrect vehicle number value");
            displayService.display("Incorrect vehicle number");
            return null;
        }
    }

    /**
     * Method for return the Next parking number, if parking have free spot
     *
     * @return The Parking spot for the user
     */
    public ParkingSpot getNextParkingNumberIfAvailable() {
        int parkingNumber;
        ParkingSpot parkingSpot = null;
        try {
            displayMenuVehicleType();
            ParkingType parkingType = getVehichleType();

            parkingNumber = parkingSpotDAO.getNextAvailableSlot(parkingType);
            logger.debug("Vehicule type choose : " + parkingType + " parking number : " + parkingNumber);
            parkingSpot = new ParkingSpot(parkingNumber, parkingType, true);
        } catch (NoSpotAvailableException e) {
            logger.warn(e.getMessage());
            displayService.display("Sorry do not have place for your " + e.getParkingType().name());
        } catch (IllegalArgumentException ex) {
            logger.warn("Error on input vehicle number");
            displayService.display(ex.getMessage());
        }
        return parkingSpot;
    }

    /**
     * Wait for user type's vehicle choice
     *
     * @return the type of parking
     * @throw IllegalArgumentException if the input number is incorrect or not implements
     */
    private ParkingType getVehichleType() throws IllegalArgumentException {
        int input = inputReaderUtil.readSelection();
        return Arrays.stream(ParkingType.values())
                .filter(parkingType -> parkingType.ordinal() == (input - 1))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Vehicle type not found"));
    }

    /**
     * Display menu for user choose his vehicle type
     */
    private void displayMenuVehicleType() {
        StringBuilder menu = new StringBuilder("Please select vehicle type from menu\n");
        Arrays.stream(ParkingType.values())
                .sorted(Comparator.comparing(Enum::ordinal))
                .forEach(parkingType -> menu.append(parkingType.ordinal() + 1).append(" ").append(parkingType.name()).append("\n"));
        System.out.println(menu.toString());
    }

    /**
     * Method for register a vehicle leaving the parking
     */
    public void processExitingVehicle() {
        try {
            String vehicleRegNumber = getVehichleRegNumber();
            if (vehicleRegNumber != null && vehicleRegNumber.length() > 0) {
                Ticket ticket = ticketDAO.getTicketToPaid(vehicleRegNumber);
                if (ticket != null) {
                    ticket.setOutTime(LocalDateTime.now());
                    ticket.getParkingSpot().setAvailable(true);
                    fareCalculatorService.calculateFare(ticket);
                    if (ticketDAO.updateTicket(ticket)) {
                        parkingSpotDAO.updateParking(ticket.getParkingSpot());
                        displayByeMessage(ticket);
                    } else {
                        logger.warn("Impossible to update the ticket for vehicle number " + vehicleRegNumber);
                        displayService.display("Unable to update ticket information. Error occurred");
                    }
                } else {
                    logger.warn("Impossible to get the ticket for vehicle number " + vehicleRegNumber);
                    displayService.display("Unable to find ticket information. Error occurred");
                }
            }
        } catch (IllegalArgumentException e) {
            logger.error("Unable to process exiting vehicle : " + e.getMessage(), e);
        }
    }

    /**
     * Display a message when the user leave the parking
     *
     * @param ticket User's ticket
     */
    private void displayByeMessage(Ticket ticket) {
        String message = "Please pay the parking fare:" + ticket.getPrice() + "\n" +
                "Recorded out-time for vehicle number:" + ticket.getVehicleRegNumber() + " is:" + ticket.getOutTime();
        displayService.display(message);
    }
}
