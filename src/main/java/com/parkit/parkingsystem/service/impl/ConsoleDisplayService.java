package com.parkit.parkingsystem.service.impl;

import com.parkit.parkingsystem.service.contract.DisplayService;

/**
 * Console implementation for {@link DisplayService}
 */
public class ConsoleDisplayService implements DisplayService {
    @Override
    public void display(String toDisplay) {
        System.out.println(toDisplay);
    }
}
