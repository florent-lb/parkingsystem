package com.parkit.parkingsystem;

import com.parkit.parkingsystem.service.impl.RecurrentFeeCalculator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.parkit.parkingsystem.service.impl.RecurrentFeeCalculator.FEE_PERCENTAGE_RECURRENT_USER;
import static org.assertj.core.api.Assertions.assertThat;

public class RecurrentFeeCalculatorTest
{
    private RecurrentFeeCalculator recurrentFeeCalculator;

    @BeforeEach
    public void setup()
    {
        recurrentFeeCalculator = new RecurrentFeeCalculator();
    }

    @Test
    @DisplayName("Calculate fee is ok for 5%")
    public void calculateFee_whenUserHaveRecurentFee_shouldHaveNewFarePrice() {
        //GET
        double price = 10.0d;
        //WHEN
        double newPrice = recurrentFeeCalculator.calculate(price);
       //ASERT

        double assertPrice  = (1 - (FEE_PERCENTAGE_RECURRENT_USER/100)) * price;
        assertThat(newPrice)
                .isEqualTo(assertPrice);


    }

}
