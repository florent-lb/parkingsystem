package com.parkit.parkingsystem.integration.service;

import com.parkit.parkingsystem.integration.config.DataBaseTestConfig;
import lombok.RequiredArgsConstructor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Use it for initialise database for tests
 */
@RequiredArgsConstructor
public class DataBasePrepareService {

    private final DataBaseTestConfig dataBaseTestConfig;

    /**
     * Clean and insert a values for tests
     */
    public void clearDataBaseEntries() {
        Connection connection = null;
        try {
            connection = dataBaseTestConfig.getConnection();

            //set parking entries to available
            PreparedStatement psUpdate = connection.prepareStatement("update parking set available = true");
            psUpdate.execute();
            //clear ticket entries;
            PreparedStatement psTruncate = connection.prepareStatement("truncate table ticket");
            psTruncate.execute();
            //Add a vehicle in parking
            final int parkingSpot = 1;
            final LocalDateTime inTime = LocalDateTime.now().minusHours(2).minusMinutes(1);
            final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME;
            PreparedStatement psAddVehicle = connection.prepareStatement("INSERT INTO ticket (PARKING_NUMBER, VEHICLE_REG_NUMBER, PRICE, IN_TIME) " +
                    " VALUES ("+parkingSpot+",'AAAAA',0,'"+ dateTimeFormatter.format(inTime)+"')" );
            psAddVehicle.executeUpdate();

            PreparedStatement psAddVehicleOut = connection.prepareStatement("INSERT INTO ticket (PARKING_NUMBER, VEHICLE_REG_NUMBER, PRICE, IN_TIME,OUT_TIME) " +
                    " VALUES ("+parkingSpot+",'AAAAA',0,'"+ dateTimeFormatter.format(inTime)+"','"+ dateTimeFormatter.format(inTime.plusHours(1))+"')"  );
            psAddVehicleOut.executeUpdate();

            PreparedStatement psAddVehicleUpdateParking = connection.prepareStatement("UPDATE parking SET AVAILABLE = 0 WHERE PARKING_NUMBER = " + parkingSpot );
            psAddVehicleUpdateParking.executeUpdate();
            dataBaseTestConfig.closePreparedStatement(psAddVehicleUpdateParking);
            dataBaseTestConfig.closePreparedStatement(psAddVehicle);
            dataBaseTestConfig.closePreparedStatement(psTruncate);
            dataBaseTestConfig.closePreparedStatement(psUpdate);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dataBaseTestConfig.closeConnection(connection);
        }
    }


}
