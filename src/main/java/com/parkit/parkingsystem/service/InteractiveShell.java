package com.parkit.parkingsystem.service;

import com.parkit.parkingsystem.config.DataBaseConfig;
import com.parkit.parkingsystem.dao.impl.ParkingSpotDAO;
import com.parkit.parkingsystem.dao.impl.TicketDAO;
import com.parkit.parkingsystem.service.contract.DisplayService;
import com.parkit.parkingsystem.service.impl.RecurrentFeeCalculator;
import com.parkit.parkingsystem.util.InputReaderUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Menu controller for the user
 */
public class InteractiveShell {

    private static final Logger logger = LogManager.getLogger("InteractiveShell");

    private final DisplayService displayService;
    private InputReaderUtil inputReaderUtil;
    private DataBaseConfig dbConfig;

    public InteractiveShell(InputReaderUtil inputReader, DisplayService displayService, DataBaseConfig dbConfig) {
        inputReaderUtil = inputReader;
        this.displayService = displayService;
        this.dbConfig = dbConfig;
    }

    /**
     * Method for load the principal menu
     * Leaved if the user choose 3 in the console
     */
    public void loadInterface() {

        displayService.display("Welcome to Parking System!");

        boolean continueApp = true;

        ParkingSpotDAO parkingSpotDAO = new ParkingSpotDAO(dbConfig);
        TicketDAO ticketDAO = new TicketDAO(dbConfig);
        FareCalculatorService fareCalculatorService = new FareCalculatorService(new RecurrentFeeCalculator());

        ParkingService parkingService = new ParkingService(inputReaderUtil, parkingSpotDAO, ticketDAO, fareCalculatorService, displayService);

        while (continueApp) {
            loadMenu();
            int option = inputReaderUtil.readSelection();
            switch (option) {
                case 1: {
                    parkingService.processIncomingVehicle();
                    break;
                }
                case 2: {
                    parkingService.processExitingVehicle();
                    break;
                }
                case 3: {
                    continueApp = false;
                    break;
                }
                default:
                    displayService.display("Unsupported option. Please enter a number corresponding to the provided menu");
            }
        }

    }

    /**
     * Use for display the main menu with the display service
     */
    private void loadMenu() {
        String menu = "Please select an option. Simply enter the number to choose an action\n" +
                "\t1 New Vehicle Entering - Allocate Parking Space\n" +
                "\t2 Vehicle Exiting - Generate Ticket Price\n" +
                "\t3 Shutdown System\n";
        displayService.display(menu);
    }

}
