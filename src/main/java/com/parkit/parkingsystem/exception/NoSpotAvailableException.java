package com.parkit.parkingsystem.exception;

import com.parkit.parkingsystem.constants.ParkingType;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Throw when no parking spot is available for this type of vehicle
 */
@RequiredArgsConstructor
public class NoSpotAvailableException extends RuntimeException
{
    @NonNull
    @Getter
    private final ParkingType parkingType;

    @Override
    public String getMessage() {
        return "No spot available for " + parkingType.name() + " type";
    }
}
